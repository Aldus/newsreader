## NewsReader
Pagemodule to handel XML and RDF/RSS (0.9x, 1.0, 2.0) newsfeeds.  
For LEPTON-CMS 4.0.

### Preface
Just an old module, original started by Robert Hase about 2005 for WebsiteBaker.  
Adapt for LEPTON-CMS.
_Nothing more, nothing less._

#### Require
- PHP >= 7.1.x
- LEPTON-CMS >= 4.0

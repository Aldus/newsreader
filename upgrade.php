<?php

/**
 *
 * @category        page
 * @package         newsreader
 * @author          Robert Hase, Matthias Gallas, Dietrich Roland Pehlke (last)
 * @license         http://www.gnu.org/licenses/gpl.html
 * @platform        LEPTON-CMS IV
 * @requirements    PHP >= 7.1
 * @version         1.0.1
 * @lastmodified    Sep 2018 
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$query = "show fields from `".TABLE_PREFIX."mod_newsreader`";

$result = $database->query ( $query );

if ($database->is_error() ) {

	$admin->print_error( $database->get_error() );

} else {
	
	$fields = array(
		'use_utf8_encode'	=> "INT NOT NULL DEFAULT 0",
		'own_dateformat'	=> "VARCHAR(100) NOT NULL DEFAULT ''"
	);
	
	while ( $data = $result->fetchRow( MYSQL_ASSOC ) ) {
		foreach($fields as $look_up_field => &$options) {
			if ($data['Field'] == $look_up_field) $options = "";
		}
	}

	$errors = array();
	
	foreach($fields as $look_up_field => &$options) {
		if ($options != "") {
			$database->query( "ALTER TABLE `".TABLE_PREFIX."mod_newsreader` ADD `".$look_up_field."` ".$options );
			
			if ( $database->is_error() ) $errors[] =$database->get_error();	
		}
	}

	if ( true === count($errors) > 0 ) {

		$admin->print_error( implode("\n", $errors ) );

	} else {

		$admin->print_success( "Update table for module 'newsreader' with success." );
	}
	
}

?>
<?php

/**
 *
 * @category        page
 * @package         newsreader
 * @author          Robert Hase, Matthias Gallas, Dietrich Roland Pehlke (last)
 * @license         http://www.gnu.org/licenses/gpl.html
 * @platform        LEPTON-CMS IV
 * @requirements    PHP >= 7.1
 * @version         1.0.1
 * @lastmodified    Sep 2018 
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$oNEWSREADER = newsreader::getInstance();
$MOD_NEWSREADER = $oNEWSREADER->language;

// output
$out = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>WB Newsreader help</title>
		<style type="text/css">
			table {
				font-family: Verdana, sans-serif;
				font-size: 12px;
				line-height: 1.3 em;
				width: 100%;
			}
			.colone {
				font-weight: bold;
				background-color: #009900;
				color: #FFFFFF;
				width: 200px;
				padding-left: 10px;
				height: 24px;
			}
			.coltwo {
				background-color: #DDDDDD;
				color: #000000;
				padding-left: 10px;
				height: 24px;
			}
		</style>
	</head>
';
echo $out;


$out = '
<body>
	<table>
		<tr>
			<td class="colone">' .$MOD_NEWSREADER['TEXT']['RSS_URI']. '</td>
			<td class="coltwo">' .$MOD_NEWSREADER['MSG']['RSS_URI']. '</td>
		</tr>
		<tr>
			<td class="colone">' .$MOD_NEWSREADER['TEXT']['CYCLE']. '</td>
			<td class="coltwo">' .$MOD_NEWSREADER['MSG']['CYCLE']. '</td>
		</tr>
		<tr>
			<td class="colone">' .$MOD_NEWSREADER['TEXT']['LAST_UPDATED']. '</td>
			<td class="coltwo">' .$MOD_NEWSREADER['MSG']['LAST_UPDATED']. '</td>
		</tr>
		<tr>
			<td class="colone">' .$MOD_NEWSREADER['TEXT']['OWN_DATEFORMAT']. '</td>
			<td class="coltwo">' .$MOD_NEWSREADER['MSG']['OWN_DATEFORMAT']. '</td>
		</tr>
		<tr>
			<td class="colone">' .$MOD_NEWSREADER['TEXT']['SHOW_IMAGE']. '</td>
			<td class="coltwo">' .$MOD_NEWSREADER['MSG']['SHOW_IMAGE']. '</td>
		</tr>
		<tr>
			<td class="colone">' .$MOD_NEWSREADER['TEXT']['SHOW_DESCRIPTION']. '</td>
			<td class="coltwo">' .$MOD_NEWSREADER['MSG']['SHOW_DESCRIPTION']. '</td>
		</tr>
		<tr>
			<td class="colone">' .$MOD_NEWSREADER['TEXT']['MAX_ITEMS']. '</td>
			<td class="coltwo">' .$MOD_NEWSREADER['MSG']['MAX_ITEMS']. '</td>
		</tr>
		<tr>
			<td class="colone">' .$MOD_NEWSREADER['TEXT']['CODING']. '</td>
			<td class="coltwo">' .$MOD_NEWSREADER['MSG']['CODING']. '</td>
		</tr>
		<tr>
			<td class="colone">' .$MOD_NEWSREADER['TEXT']['USE_UTF8_ENCODING']. '</td>
			<td class="coltwo">' .$MOD_NEWSREADER['MSG']['USE_UTF8_ENCODING']. '</td>
		</tr>
	</table>
</body>
</html>
';

echo $out;

?>
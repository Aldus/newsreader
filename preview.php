<?php

/**
 *
 * @category        page
 * @package         newsreader
 * @author          Robert Hase, Matthias Gallas, Dietrich Roland Pehlke (last)
 * @license         http://www.gnu.org/licenses/gpl.html
 * @platform        LEPTON-CMS IV
 * @requirements    PHP >= 7.1
 * @version         1.0.1
 * @lastmodified    Sep 2018 
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$oNEWSREADER = newsreader::getInstance();

$MOD_NEWSREADER = $oNEWSREADER->language;

include_once('./functions.php');
	
// create and set object newsfeed
include_once('./newsparser.php');

$px = new RSS_feed();
$px->Set_Limit($_REQUEST['MAX_ITEMS']); 
$px->Show_Image($_REQUEST['SHOW_IMAGE']); 
$px->Show_Description($_REQUEST['SHOW_DESCRIPTION']);
$px->Set_URL($_REQUEST['RSS_URI']);

$nf = array();	
$nf['show_image'] = $_REQUEST['SHOW_IMAGE'];
$nf['show_desc'] = $_REQUEST['SHOW_DESCRIPTION'];
$nf['coding_from'] = $_REQUEST['CODE_FROM'];
$nf['coding_to'] = $_REQUEST['CODE_TO'];
$nf['use_utf8_encoding'] = $_REQUEST['USE_UTF8ENCODE'];
$nf['own_dateformat'] = $_REQUEST['OWN_DATEFORMAT'];

// get newsfeed contents
$nf['content'] = $px->Get_Results( $nf['use_utf8_encoding'] );
$nf['ch_title'] = $px->channel['title'];
$nf['ch_link'] = isset($px->channel['link']) ? $px->channel['link'] : "";
$nf['ch_desc'] = $px->channel['desc'];
$nf['img_title'] = $px->image['title'];
$nf['img_uri'] = isset($px->image['url']) ? $px->image['url'] : ""; // URI to image/logo
$nf['img_link'] = isset($px->image['link']) ? $px->image['link'] : "";

/**	*************
 *	Date and time
 */
$oCDate = lib_lepton::getToolInstance("datetools");
	
$oCDate->set_core_language( LANGUAGE );
	
if($nf['own_dateformat'] != "") {
	$oCDate->format = $nf['own_dateformat'];
} else {
	$oCDate->format = $oCDate->CORE_date_formats[ DATE_FORMAT ] ." - ".$oCDate->CORE_time_formats[ TIME_FORMAT ];
}
	
$last_update = $oCDate->toHTML( time() + (defined('TIMEZONE') ? TIMEZONE : 0) );

$out = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>WB Newsreader</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="content-language" content="en" />
		<style type="text/css">
			body{
				font-family: Verdana, sans-serif;
				font-size: 13px;
				line-height: 1.4em;
			}
			h1 {
				fontsize: 14px;
			}
			td {
				background-color: #336699;
				color: #FFFFFF;
				padding-left: 10px;
				height: 20px;
			}
			table#configuration tr th,
			table#ressourcen tr th {
				text-align: left;
				padding-left: 10px;
				background-color: #CCCCCC;
			}
			table#configuration tr th:first-child {
				width: 200px;
			}
			table#ressourcen tr th:first-child {
				width: 150px;
			}
			.newsreader {
			}
			.nr_description {
				font-weight: bold;
			}
			.nr_content {
			}
			.nr_content ul {
			}
			.nr_content li {
				margin-bottom: 1em;
				list-style: circle;
			}
			.nr_itemdesc {
			}

			.discreet {
				font-size:90%;
			}
 		</style>
	</head>
	<body>
		<h1>' . $MOD_NEWSREADER['HEAD']['CONFIG_DISPL'] . '</h1>
		<br />
		<table width="100%" id="configuration">
			<tr>
				<th>' . $MOD_NEWSREADER['TEXT']['Configuration'] . '</th>
				<th>' . $MOD_NEWSREADER['TEXT']['Request'] . '</th>
			</tr>
			<tr>
				<td>' . $MOD_NEWSREADER['TEXT']['RSS_URI'] . '</td>
				<td>' . $_REQUEST['RSS_URI'] . '</td>
			</tr>
			<tr>
				<td>' . $MOD_NEWSREADER['TEXT']['SHOW_IMAGE'] . '</td>
				<td>' . $_REQUEST['SHOW_IMAGE'] . '</td>
			</tr>
			<tr>
				<td>' . $MOD_NEWSREADER['TEXT']['SHOW_DESCRIPTION'] . '</td>
				<td>' . $_REQUEST['SHOW_DESCRIPTION'] . '</td>
			</tr>
			<tr>
				<td>' . $MOD_NEWSREADER['TEXT']['MAX_ITEMS'] . '</td>
				<td>' . $_REQUEST['MAX_ITEMS'] . '</td>
			</tr>
			<tr>
				<td>' . $MOD_NEWSREADER['TEXT']['CODING'] . '</td>
				<td>' . $TEXT['FROM'] . ': ' . $_REQUEST['CODE_FROM'] . ' ' . $TEXT['TO'] . ': ' . $_REQUEST['CODE_TO'] . '</td>
			</tr>
			<tr>
				<td>' . $MOD_NEWSREADER['TEXT']['USE_UTF8_ENCODING'] . '</td>
				<td>' . ($nf['use_utf8_encoding'] == 1 ? $TEXT['YES'] : $TEXT['NO']) . '</td>
			</tr>
			<tr>
				<td>' . $MOD_NEWSREADER['TEXT']['OWN_DATEFORMAT'] . '</td>
				<td>' . ($_REQUEST['OWN_DATEFORMAT'] != "" ? ($_REQUEST['OWN_DATEFORMAT'] . " (E.g.: " .$last_update. ")") : "" ) .'</td>
			</tr>
		</table>

		<br />

		<table width="100%" id="ressourcen">
			<tr>
				<th>' . $MOD_NEWSREADER['TEXT']['Resource'] . '</th>
				<th>' . $MOD_NEWSREADER['TEXT']['Value'] . '</th>
				<th>' . $MOD_NEWSREADER['TEXT']['Description'] . '</th>
			</tr>
			<tr>
				<td>' . $MOD_NEWSREADER['TEXT']['RSS_URI'] . '</td>
				<td>' . $px->URL . '</td>
				<td>' . $MOD_NEWSREADER['MSG']['RSS_URI'] . '</td>
			</tr>
			<tr>
				<td>' . $MOD_NEWSREADER['TEXT']['Image-URI'] . '</td>
				<td>' . $nf['img_uri'] . '<br />'. (($nf['img_uri'] != "") ? '<img src="' . $nf['img_uri'] . '" alt="logo" />' : '') .'</td>
				<td>' . $MOD_NEWSREADER['DESC']['Image-URI'] . '</td>
			</tr>
			<tr>
				<td>' . $MOD_NEWSREADER['TEXT']['Image-Title'] . '</td>
				<td>' . $nf['img_title'] . '</td>
				<td>' . $MOD_NEWSREADER['DESC']['Image-Title'] . '</td>
			</tr>
			<tr>
				<td>' . $MOD_NEWSREADER['TEXT']['Image-Link'] . '</td>
				<td>' . $nf['img_link'] . '</td>
				<td>' . $MOD_NEWSREADER['DESC']['Image-Link'] . '</td>
			</tr>
			<tr>
				<td>' . $MOD_NEWSREADER['TEXT']['Channel-Title'] . '</td>
				<td>' . $nf['ch_title'] . '</td>
				<td>' . $MOD_NEWSREADER['DESC']['Channel-Title'] . '</td>
			</tr>
			<tr>
				<td>' . $MOD_NEWSREADER['TEXT']['Channel-Desc'] . '</td>
				<td>' . $nf['ch_desc'] . '</td>
				<td>' . $MOD_NEWSREADER['DESC']['Channel-Desc'] . '</td>
			</tr>
			<tr>
				<td>' . $MOD_NEWSREADER['TEXT']['Channel-Link'] . '</td>
				<td>' . $nf['ch_link'] . '</td>
				<td>' . $MOD_NEWSREADER['DESC']['Channel-Link'] . '</td>
			</tr>
		</table>

		<br />';
		/*
		if($nf['coding_from'] != '--' && $nf['coding_to'] != '--')
		{
			include_once('./ConvertCharset.class.php');
			$NewEncoding = new ConvertCharset;
			$nf['ch_title'] = $NewEncoding->Convert($nf['ch_title'],$nf['coding_from'] , $nf['coding_to'], 0);
			$nf['ch_desc'] = $NewEncoding->Convert($nf['ch_desc'],$nf['coding_from'] , $nf['coding_to'], 0);
			$nf['content'] = $NewEncoding->Convert($nf['content'],$nf['coding_from'] , $nf['coding_to'], 0);
		}
		*/
$out .=	'<b>Output:</b>
			<hr />
			<br />
<div class="newsreader">';
if ($nf['img_link'] != "") {
$out .= '	<a href="' . $nf['img_link'] . '" title="' . $nf['img_title'] . '">
		<img src="' . $nf['img_uri'] . '" alt="logo" title="' . $nf['img_title'] . '" border="0" />
	</a>';
}
$out .='	<h2>' . $nf['ch_title'] . '</h2>
	<div class="nr_description">' . $nf['ch_desc'] . '</div>
	<div class="discreet">' . $MOD_NEWSREADER['TEXT']['LAST_UPDATED'] . ': ' . date("Y-m-d H:i:s", time() + (defined('TIMEZONE') ? TIMEZONE : 0)) . '</div>
	<div class="nr_content">' .
		$nf['content'] .
'	</div>
</div>

	</body>
</html>
';

echo $out;

?>
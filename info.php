<?php

/**
 *
 * @category        page
 * @package         newsreader
 * @author          Robert Hase, Matthias Gallas, Dietrich Roland Pehlke (last)
 * @license         http://www.gnu.org/licenses/gpl.html
 * @platform        LEPTON-CMS IV
 * @requirements    PHP >= 7.1
 * @version         1.0.1
 * @lastmodified    Sep 2018 
 *
 */

$module_directory	= 'newsreader';
$module_name		= 'Newsreader';
$module_function	= 'page';
$module_version		= '1.0.1';
$module_platform 	= '4.0';
$module_author		= 'Robert Hase, adm_prg[AT]muc-net.de, Matthias Gallas, Dietrich Roland Pehlke (last)';
$module_license		= 'GNU General Public License';
$module_description	= 'This module handels XML and RDF/RSS (0.9x, 1.0, 2.0) newsfeeds.';
$module_guid		= '913D804E-E254-4E09-A8F4-9C78519EF13D';
?>
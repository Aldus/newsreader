<?php

/**
 *
 * @category        page
 * @package         newsreader
 * @author          Robert Hase, Matthias Gallas, Dietrich Roland Pehlke (last)
 * @license         http://www.gnu.org/licenses/gpl.html
 * @platform        LEPTON-CMS IV
 * @requirements    PHP >= 7.1
 * @version         1.0.1
 * @lastmodified    Sep 2018 
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

include_once(LEPTON_PATH . '/modules/newsreader/functions.php');

////////////////////////////////////////////////////////////////////////
// don't change anything below until you're knowing what you're doing //
////////////////////////////////////////////////////////////////////////

$sqlrow = array();
$database->execute_query(
    "SELECT * FROM `".TABLE_PREFIX."mod_newsreader` WHERE `section_id` = ".$section_id,
    true,
    $sqlrow,
    false
);

$last_update = $sqlrow['last_update'] + $sqlrow['cycle'];
if (!defined('DATETIME'))
{
    define('DATETIME', DATE_FORMAT . ' ' . TIME_FORMAT);
}

if( ( ( $sqlrow['last_update'] == 0 || strlen($sqlrow['content']) == 0) ) || $last_update < time() ) {
	output(
		update(
			$sqlrow['uri'],
			$section_id,
			$sqlrow['show_image'],
			$sqlrow['show_desc'],
			$sqlrow['show_limit'],
			$sqlrow['coding_from'],
			$sqlrow['coding_to'],
			$sqlrow['use_utf8_encode'],
			$sqlrow['own_dateformat']
	));
}
else {
	output( $sqlrow );
}

?>
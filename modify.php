<?php

/**
 *
 * @category        page
 * @package         newsreader
 * @author          Robert Hase, Matthias Gallas, Dietrich Roland Pehlke (last)
 * @license         http://www.gnu.org/licenses/gpl.html
 * @platform        LEPTON-CMS IV
 * @requirements    PHP >= 7.1
 * @version         1.0.1
 * @lastmodified    Sep 2018 
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$oNEWSREADER = newsreader::getInstance();
$MOD_NEWSREADER = $oNEWSREADER->language;

global $admin;

include_once(LEPTON_PATH . '/modules/newsreader/functions.php');

// include core functions to edit the optional module CSS files (frontend.css, backend.css)
include_once(LEPTON_PATH .'/framework/summary.module_edit_css.php');


$fields = array(
	"uri","cycle","last_update","show_image","show_desc","show_limit","coding_from","coding_to","use_utf8_encode","own_dateformat"
);

$sqlrow = array();
$database->execute_query(
	"SELECT `".(implode("`,`",$fields))."`  FROM `".TABLE_PREFIX."mod_newsreader` WHERE `section_id` = '".$section_id."'",
	true,
	$sqlrow,
	false
);

$uri = $sqlrow['uri'];
$cycle = $sqlrow['cycle'];
$datetime = DATE_FORMAT . ' ' . TIME_FORMAT;
$last_update = $sqlrow['last_update'];
$show_image = $sqlrow['show_image'];
$show_imageCkd = ($show_image == 1) ? 'checked="checked"' : "";
$show_desc = $sqlrow['show_desc'];
$show_descCkd = ($show_desc == 1) ? 'checked="checked"' : "";
$show_limit = $sqlrow['show_limit'];	
$optionFrom = $sqlrow['coding_from'];
$optionTo = $sqlrow['coding_to'];
$use_utf8_encoding = $sqlrow['use_utf8_encode']==1 ? "checked='checked'" : "";
$own_dateformat = $sqlrow['own_dateformat'];

/**
 *	include the button to edit the optional module CSS files (function added with WB 2.7)
 *	Note: CSS styles for the button are defined in backend.css (div class="mod_moduledirectory_edit_css")
 *	Place this call outside of any <form></form> construct!
 */
if(function_exists('edit_module_css'))
{
    ob_start();
	edit_module_css('newsreader');
	$sCSSForm = ob_get_clean();
} else {
    $sCSSForm = "";
}

/**	*************
 *	Date and time
 */
$oCDate = lib_lepton::getToolInstance("datetools");
	
$oCDate->set_core_language( LANGUAGE );
	
if($own_dateformat != "") {
	$oCDate->format = $own_dateformat;
} else {
	$oCDate->format = $oCDate->CORE_date_formats[ DATE_FORMAT ] ." - ".$oCDate->CORE_time_formats[ TIME_FORMAT ];
}
	
$last_update = $oCDate->toHTML( $last_update + (defined('TIMEZONE') ? TIMEZONE : 0) );

$arrOptions = readCharsets();

$select_from_options = "";
foreach($arrOptions as &$option){
	$select_from_options .= '<option value="'.$option.'"'; 
	$select_from_options .= ($option == $optionFrom) ? ' selected="selected">' : ">" ;
	$select_from_options .= $option . '</option>';
}

$select_to_options = "";
foreach($arrOptions as &$option){
	$select_to_options .= '<option value="'.$option.'"'; 
	$select_to_options .= ($option == $optionTo) ? ' selected="selected">' : ">" ;
	$select_to_options .= $option . '</option>';
}

$form_values = array(
	'LEPTON_URL'	=> LEPTON_URL,
	'SECTION_ID'	=> $section_id,
	'PAGE_ID'		=> $page_id,
	'SQLTYPE'		=> 'UPDATE',
	'TEXT_RSS_URI'	=> $MOD_NEWSREADER['TEXT']['RSS_URI'],
	'URI'			=> $uri,
	'FTAN'			=> method_exists($admin, "getFTAN") ? $admin->getFTAN() : "",
	'TEXT_CYCLE'	=> $MOD_NEWSREADER['TEXT']['CYCLE'],
	'CYCLE'			=> $cycle,
	'TEXT_LAST_UPDATED'	=> $MOD_NEWSREADER['TEXT']['LAST_UPDATED'],
	'LAST_UPDATE'		=>  $last_update,
	'TEXT_SHOW_IMAGE'	=> $MOD_NEWSREADER['TEXT']['SHOW_IMAGE'],
	'SHOW_IMAGE'		=> $show_image,	
	'SHOW_IMAGECKD'	=> $show_imageCkd,
	'TEXT_SHOW_DESCRIPTION'	=> $MOD_NEWSREADER['TEXT']['SHOW_DESCRIPTION'],
	'SHOW_DESC'	=> $show_desc,
	'SHOW_DESCCKD'	=> $show_descCkd,
	'TEXT_MAX_ITEMS'	=> $MOD_NEWSREADER['TEXT']['MAX_ITEMS'],
	'SHOW_LIMIT'	=> $show_limit,
	'TEXT_CODING'	=> $MOD_NEWSREADER['TEXT']['CODING'],
	'TEXT_FROM'		=> strtolower($TEXT['FROM']),
	'SELECT_FROM'	=> $select_from_options,				#1
	'TEXT_TO'		=> strtolower($TEXT['TO']),
	'SELECT_TO'		=> $select_to_options,					#2
	'TEXT_USE_UTF8_ENCODING'	=> $MOD_NEWSREADER['TEXT']['USE_UTF8_ENCODING'],
	'TEXT_OWN_DATEFORMAT'	=> $MOD_NEWSREADER['TEXT']['OWN_DATEFORMAT'],
	'OWN_DATEFORMAT'	=> $own_dateformat,
	'USE_UTF8_ENCODING'	=> $use_utf8_encoding,
	'TEXT_SAVE'	=> $TEXT['SAVE'],
	'TEXT_CANCEL' => $TEXT['CANCEL'],
	'TEXT_PREVIEW' => $MOD_NEWSREADER['TEXT']['PREVIEW'],
	'leptoken'      => get_leptoken(),
	'CSS_FORM'      => $sCSSForm
);

$oTWIG = lib_twig_box::getInstance();
$oTWIG->registerModule("newsreader");

echo $oTWIG->render(
	"@newsreader/modify.lte",
	$form_values
);
?>
<?php

/**
 *
 * @category        page
 * @package         newsreader
 * @author          Robert Hase, Matthias Gallas, Dietrich Roland Pehlke (last)
 * @license         http://www.gnu.org/licenses/gpl.html
 * @platform        LEPTON-CMS IV
 * @requirements    PHP >= 7.1
 * @version         1.0.1
 * @lastmodified    Sep 2018 
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

// Tells script to update when this page was last updated
$update_when_modified = true;
// Include WB admin wrapper script
$admin_header = false; // suppress to print the header, so no new FTAN will be set
require(LEPTON_PATH.'/modules/admin.php');
$admin->print_header();
     
$lang_file = LEPTON_PATH . '/modules/newsreader/languages/' . LANGUAGE . '.php';
require_once( file_exists($lang_file) ? $lang_file : LEPTON_PATH . '/modules/newsreader/languages/EN.php' );

//require_once(LEPTON_PATH."/framework/class.validate.request.php");

//$oVal = new lepton_validate_request();
$oVal = LEPTON_request::getInstance();
//$oVal->strict_looking_inside = "post";

$all_names = array (
	'uri'			=> array ('type' => 'str',	'default' => NULL,	'range' =>""),
	'cycle'			=> array ('type' => 'int+', 'default' => 86400, 'range' => array('min'=> 14400, 'max'=> 999999)),
	'show_image'	=> array ('type' => 'int+', 'default' => 0, 'range' => array('min'=> 0, 'max'=> 1)),
	'show_desc'		=> array ('type' => 'int+', 'default' => 0, 'range' => array('min'=> 0, 'max'=> 1)),
	'show_limit'	=> array ('type' => 'int+',	'default' => "10",	'range' => array('min'=> 1, 'max'=> 50)),
	'coding_from'		=> array ('type' => 'str',  'default' => '--', 'range' => ""),
	'coding_to' 		=> array ('type' => 'str', 'default' => '--', 'range' => ""),
	'use_utf8_encode'	=> array ('type' => 'int+', 'default' => 0, 'range' => array('min'=> 0, 'max'=> 1)),
	'own_dateformat'	=> array ('type' => 'str',	'default' => NULL,	'range' =>"")
);

$all_values = array ();

foreach($all_names as $item=>&$options) 
	$all_values[$item] = $oVal->get_request($item, $options['default'], $options['type'], $options['range']);

$database->build_and_execute(
	'update',
	TABLE_PREFIX."mod_newsreader",
	$all_values,
	'section_id = '. $section_id
);		

// get the newsfeed and save it

include_once(LEPTON_PATH . '/modules/newsreader/functions.php');
$result = update(
	$all_values['uri'],
	$section_id,
	$all_values['show_image'],
	$all_values['show_desc'],
	$all_values['show_limit'],
	$all_values['coding_from'],
	$all_values['coding_to'],
	$all_values['use_utf8_encode']
);

if($database->is_error()) {
	$admin->print_error(mysql_error() . " ".$query, $js_back);
} else {
	if (!is_array( $result) ) {
			$admin->print_error( $result . "<br />".$MOD_NEWSREADER_TEXT['RECORDS UNTOUCHED'], $js_back);
	
	} else {
		$admin->print_success($MESSAGE['PAGES_SAVED'], ADMIN_URL.'/pages/modify.php?page_id='.$page_id);
	}
}

$admin->print_footer();

?>
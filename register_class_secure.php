<?php

/**
 *
 * @category        page
 * @package         newsreader
 * @author          Robert Hase, Matthias Gallas, Dietrich Roland Pehlke (last)
 * @license         http://www.gnu.org/licenses/gpl.html
 * @platform        LEPTON-CMS IV
 * @requirements    PHP >= 7.1
 * @version         1.0.1
 * @lastmodified    Sep 2018 
 *
 */

$files_to_register = array(
	'save.php',
	'precheck.php',
	'preview.php',
	'help.php'
);

LEPTON_secure::getInstance()->accessFiles( $files_to_register );

?>
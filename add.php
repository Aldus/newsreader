<?php

/**
 *
 * @category        page
 * @package         newsreader
 * @author          Robert Hase, Matthias Gallas, Dietrich Roland Pehlke (last)
 * @license         http://www.gnu.org/licenses/gpl.html
 * @platform        LEPTON-CMS IV
 * @requirements    PHP >= 7.1
 * @version         1.0.1
 * @lastmodified    Sep 2018 
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$fields = array(
	'last_update'	=> Time(),
	'content'		=> "",
	'ch_title'		=> "",
	'ch_link'		=> "",
	'ch_desc'		=> "",
	'img_title'		=> "",
	'img_uri'		=> "",
	'img_link'		=> "",
	'use_utf8_encode' => 0,
	'show_limit'	=> 15,
	'cycle'			=> 86400,
	'coding_from'	=> '--',
	'coding_to'		=> '--',
	'show_image'	=> 1,
	'show_desc'		=> 1,
	'section_id'	=> $section_id,
	'page_id'		=> $page_id
);

$database->build_and_execute(
	'insert',
	TABLE_PREFIX . 'mod_newsreader',
	$fields
);

?>